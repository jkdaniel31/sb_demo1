package com.sbjenkinsdemo1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sbjenkinsdemo1Application {

	public static void main(String[] args) {
		SpringApplication.run(Sbjenkinsdemo1Application.class, args);
	}

}
