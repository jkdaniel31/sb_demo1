package com.sbjenkinsdemo1.controller;

import java.util.ArrayList;
import java.util.List;


import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


import com.sbjenkinsdemo1.model.User;

@RestController
public class UserController {

	@GetMapping(value = "/empall", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<User> getUsers() {
		List<User> users = new ArrayList();
		users.add(new User(12, "One", "Two"));
		users.add(new User(34, "Three", "Four"));
		users.add(new User(56, "Five", "Six"));
		return users;
	}

	@GetMapping(value = "/emp/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public User getOneUser(@PathVariable Integer id) {
		User user = null;
		if(id ==10 )
		{
			user = new User(10, "One", "Zero");
		}
		if(id ==20 )
		{
			user = new User(20, "Two", "Zero");
		}
		return user;
	}

}
